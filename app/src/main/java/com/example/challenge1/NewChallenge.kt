package com.wisnu.dasarkotin

import java.util.*

fun main(args: Array<String>) {
    println(StockPicker(arrayOf(44, 30, 24, 32, 35, 30, 40, 38, 15)))
    println(StockPicker(arrayOf(10, 9, 8, 2)))

    println(isAnagrams("mycar", "camry"))
    println(isAnagrams("Hello", "hello"))
    println(isAnagrams("anagram", "margana"))
    println(isAnagrams("Raden", "Denah"))
}

fun StockPicker(arr: Array<Int>): Int {

    var max_profit = -1;
    var buy_price = 0;
    var sell_price = 0;

    var change_buy_index = true;


    for (i in 0 until arr.size -1) {


        sell_price = arr[i+1]

        if (change_buy_index) { buy_price = arr[i] }

        if (sell_price < buy_price) {
            change_buy_index = true
            continue
        }

        else {
            val temp_profit = sell_price - buy_price
            if (temp_profit > max_profit) { max_profit = temp_profit }
            change_buy_index = false
        }
    }
    return max_profit
}


fun isAnagrams(str1: String, str2: String): Any {
    var hasil: String

    if (str1.length != str2.length) {
        return false
    }

    val strArray1 = str1.toCharArray()
    val strArray2 = str2.toCharArray()

    Arrays.sort(strArray1)
    Arrays.sort(strArray2)

    val sortedStr1 = String(strArray1)
    val sortedStr2 = String(strArray2)

    return sortedStr1 == sortedStr2
}