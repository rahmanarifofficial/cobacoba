class Triangle {
    fun triangle(){
        for(i in 0..8){
            for(j in 1..8-i){
                print(" ")
            }
            for(k in 0 until (2*i)-1){
                print("*")
            }
            println("")
        }
    }

    fun reversedTriangle(){
        for(i in 0..8){
            for(j in 0 .. i){
                print(" ")
            }
            for(k in 0 until 2 * (7 - i) - 1){
                print("*")
            }
            println("")
        }
    }

    fun kite(){
        this.triangle()
        this.reversedTriangle()
    }

    fun xShape(){
        for(i in 0..8){
            for(j in 0 .. i){
                print(" ")
            }
            for(k in 0 until 2 * (7 - i) - 1) {
                if (k == 0 || k == (2 * (7 - i)) - 1 - 1) {
                    print("*")
                } else {
                    print(" ")
                }
            }
            if(i<7) println("")
        }
        for(i in 0..8){
            for(j in 1..8-i){
                print(" ")
            }
            for(k in 0 until (2 * i) - 1){
                if(k == 0 || k == (2 * i) -1 - 1){
                    print("*")
                } else {
                    print(" ")
                }
            }
            println("")
        }
    }

    fun hollowTriangle(){
        for(i in 0..8){
            for(j in 1..8-i){
                print(" ")
            }
            for(k in 0 until (2*i)-1){
                if(k == 0 || i == 8){
                    print("*")
                } else if (k == 0 || k == (2*i) -1 -1){
                    print("*")
                } else {
                    print(" ")
                }
            }
            println("")
        }
    }
}

fun main(){
    val triangle = Triangle()
    println(triangle.triangle())
    println(triangle.reversedTriangle())
    println(triangle.kite())
    println(triangle.xShape())
    println(triangle.hollowTriangle())
}