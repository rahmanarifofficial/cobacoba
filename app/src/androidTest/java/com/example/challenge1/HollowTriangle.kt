
    fun printPattern(n: Int) {
        var i: Int
        var j: Int

        // Loop for rows
        i = 1
        while (i <= n) {


            // Loop for column
            j = 1
            while (j < 2 * n) {


                // For printing equal sides
                // of outer triangle
                if (j == n - i + 1
                    || j == n + i - 1
                ) {
                    print("* ")
                }
                 else if (i == n
                    || i == n - 4 && j >= n - (n - 2 * 4) && j <= n + n - 2 * 4
                ) {
                    print("* ")
                } else {
                    print(
                        " "
                                + " "
                    )
                }
                j++
            }
            print("\n")
            i++
        }
    }

    // Driver Code
    fun main(args: Array<String>) {
        val N = 6
        printPattern(N)
    }
