package com.example.challenge1

fun main() {

    var starCount = 8;
    val count = starCount * 2 - 1;

    for(i in 1..count){
        for(j in 1..count){
            if(j==i || (j==count - i + 1))
            {
                print("*");
            }
            else
            {
                print(" ");
            }
        }

        println("")
    }

}