import java.util.*


//Zein Irfansyah
fun main(args: Array<String>) {

//    NOMOR 1 =====================================
    val selisihTerbesar = ProfitMaksimum()
//    kondisi 1
    val arr = intArrayOf(44, 30, 24, 32, 35, 30, 40, 38, 15)
    println(selisihTerbesar.selisihMaksimum(arr))

//    kondisi 2
    val arr2 = intArrayOf(10, 9, 8, 2)
    println(selisihTerbesar.selisihMaksimum(arr2))

    println("\n\n=========================================\n\n")
//    NOMOR 3 ======================================
    val anagram = Anagram()

    println(anagram.isAnagrams("mycar", "camry"))
    println(anagram.isAnagrams("Hello", "hello"))
    println(anagram.isAnagrams("anagram", "margana"))
    println(anagram.isAnagrams("Raden", "Denah"))
}


class ProfitMaksimum {
    fun selisihMaksimum(arr: IntArray): Int {
        var profitMaksimum = -1
        var hargaBeli = 0
        var hargaJual = 0

        var x = true

        for(i in 0 until arr.size - 1) {
            hargaJual = arr[i + 1]
            if(x) {
                hargaBeli = arr[i]
                x = false
            }

            if (hargaJual < hargaBeli) {
                x = true
                continue
            } else {
                val profit = hargaJual - hargaBeli
                if (profit > profitMaksimum) {
                    profitMaksimum = profit
                }
                x = false
            }
        }
        return profitMaksimum
    }
}

class Anagram {
    fun isAnagrams(str1: String, str2: String): Any {
        var hasil: String

        if (str1.length != str2.length) {
            return false
        }

        val strArray1 = str1.toCharArray()
        val strArray2 = str2.toCharArray()

        Arrays.sort(strArray1)
        Arrays.sort(strArray2)

        val sortedStr1 = String(strArray1)
        val sortedStr2 = String(strArray2)


        if (sortedStr1 == sortedStr2) {
            hasil = "Anagram"
        } else {
            hasil = "Bukan Anagram"
        }

        return hasil

    }
}