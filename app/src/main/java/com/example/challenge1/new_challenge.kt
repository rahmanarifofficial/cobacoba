import java.util.*

fun main(){
    // CHALLENGE 1
    val stock = arrayOf(44, 30, 24, 32, 35, 30, 40, 38, 15)
    val stock2 = arrayOf(10, 9, 8, 2)
    println(arrayChallenge(stock))
    println(arrayChallenge(stock2))

    // CHALLENGE 2
    val num = "0851-6231-7243"
    val num2 = "+62877]6294*2312"
    val num3 = "+62877 6294 2312"
    println(formatNumber(num))

    // CHALLENGE 3
    println(isAnagram("mycar", "camry"))
    println(isAnagram("Hello", "hello"))
}

fun arrayChallenge(stock: Array<Int>): Int {
    var profit: Int = 0

    for(i in 0 until stock.size){
        for(j in 0..i){
            if(profit > 0){
                if(stock[i] - stock[j] > profit){
                    profit = stock[i] - stock[j]
                }
            } else {
                profit = stock[i] - stock[j]
            }
        }
    }
    return if(profit > 0) profit else -1
}

fun formatNumber(phone:String):String{
    val re = Regex("""\w\D""");
    var phoneNumber = phone.replace(re, "")
    return phoneNumber.replace("+", "")
}

fun isAnagram(a: String, b:String):Boolean{
    if(a.length != b.length) return false

    val stringOne = a.toCharArray()
    val stringTwo = b.toCharArray()
    Arrays.sort(stringOne)
    Arrays.sort(stringTwo)

    return String(stringOne) == String(stringTwo)
}