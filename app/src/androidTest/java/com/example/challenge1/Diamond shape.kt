object DiamondPattern {
    fun main(args: Array<String>) {
        val number: Int
        var i: Int
        var k: Int
        var count = 1
        number = 8
        count = number - 1
        k = 1
        while (k <= number) {
            i = 1
            while (i <= count) {
                print(" ")
                i++
            }
            count--
            i = 1
            while (i <= 2 * k - 1) {
                print("*")
                i++
            }
            println()
            k++
        }
        count = 1
        k = 1
        while (k <= number - 1) {
            i = 1
            while (i <= count) {
                print(" ")
                i++
            }
            count++
            i = 1
            while (i <= 2 * (number - k) - 1) {
                print("*")
                i++
            }
            println()
            k++
        }
    }
}